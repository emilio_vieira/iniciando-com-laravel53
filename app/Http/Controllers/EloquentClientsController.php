<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class EloquentClientsController extends Controller
{
    public function index(){
        $clients = Client::all();

        return view('eloquent.clients',[
            'clients'=>$clients
        ]);

    }

    //
    public function create(){
        return view('eloquent.create');
    }

    public function store(Request $request){

        $client = new Client();

        $client::create($request->all());

        return redirect()->route('eloquent.client.list');

    }

    public function delete(Request $request, $id){

        $client = new Client();
        $client::destroy($id);

        return redirect()->route('eloquent.client.list');
    }
}
