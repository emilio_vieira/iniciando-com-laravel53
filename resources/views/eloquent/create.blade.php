    <html>
<body>
<h3>Eloquent - Cadastrar Clientes</h3>



<form action="{{ route('eloquent.client.store') }}" method="post">

    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

    <label for="name">Nome</label>
    <input type="text" name="name" id="name"/>

    <label for="address">Endereço</label>
    <input type="text" name="address" id="address"/>

    <button type="submit">Enviar</button>

</form>

<a href="{{ route('eloquent.client.list') }}">Voltar</a><br><br>
</body>