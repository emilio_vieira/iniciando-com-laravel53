<h3>Eloquent Clientes</h3>

<a href="{{ route('eloquent.client.create') }}">Cadastrar</a><br><br>

<table border="1">
    <thead>
        <tr>
            <th>#</th>
            <th>name</th>
            <th>address</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($clients as $client)
            <tr>
                <td>{{ $client->id }}</td>
                <td>{{ $client->name }}</td>
                <td>{{ $client->address }}</td>
                <td align="center">
                    <a class="btn-delete" href="#" data-name="{{ $client->name }}" data-href="{{ route('eloquent.client.delete',['id'=>$client->id]) }}" data-object="{{ $client->toJson()  }}">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>

</table>

<script>

    document.addEventListener('DOMContentLoaded',function(){

        var allbtn = document.getElementsByClassName('btn-delete');

        Array.prototype.forEach.call(allbtn,function(elem){

            let name = elem.dataset.name;
            let url = elem.dataset.href;
            let clientJSON = JSON.parse(elem.dataset.object);

            elem.onclick = function(){

                if( window.confirm('Deseja realmente excluir registro '+ clientJSON.id +' - '+clientJSON.name +'?') ){
                    location.href = url;
                }

            }
        });
    });

</script>