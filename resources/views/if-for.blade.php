<?php $num = 20; ?>

<!-- uso do '@'  para Estruturas lógicas-->
@if($num < 20)
    <p>Número é menor que vinte</p>
@elseif($num==20)
    <p>Número é igual a vinte</p>
@else
    <p>Número é Maior que vinte</p>
@endif



<?php $k = 0; ?>

{{ "WHILE" }}
@while($k < 20)
    <p>Valor: {{ $k }}</p>
    <?php $k++ ?>
@endwhile

{{ "FOREACH" }}
<?php $array = [1,2,3,4,5]; ?>

@foreach($array as $value)
    <p>Chave {{ $loop->index }}, Valor: {{ $value }}</p>
@endforeach

{{ "FORELSE or ELSE FOREACH" }}
<?php $array = []; ?>

@forelse($array as $value)
    <p>Chave {{ $loop->index }}, Valor: {{ $value }}</p>
@empty
    <p>Nenhum elemento no array</p>
@endforelse



