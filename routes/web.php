<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'eloquent', 'as' => 'eloquent.'], function () {
    Route::get('clients', 'EloquentClientsController@index')->name('client.list');
    Route::get('client/create', 'EloquentClientsController@create')->name('client.create');
    Route::post('client/save', 'EloquentClientsController@store')->name('client.store');
    Route::get('client/delete/{id}', 'EloquentClientsController@delete')->name('client.delete');
});

Route::group(['prefix' => '', 'as' => 'site.'], function () {
    /* Usando controllers - 'Controller@action' */
    Route::get('client', 'SiteClientsController@create');
    Route::post('client/save', 'SiteClientsController@store')->name('client.store');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    /* Usando controllers - 'Controller@action' */
    Route::get('client', 'ClientsController@create');
    Route::post('client/save', 'ClientsController@store')->name('client.store');
});





/*
Route::get('client', function () {
    return view('client');
});

Route::post('client', function (\Illuminate\Http\Request $request) {
//    return $request->get('value');
//    var_dump(json_encode($_POST));
    $posted = json_encode($request->all());
    return "Dados Postados pelo formulario_csrf: <hr/> {$posted}";
})->name('client.store');

Route::get('client/{id}/{name?}', function ($id, $name = 'Jesus Cristo') {

    return view('client-name')
        ->with('id', $id)
        ->with('name', $name);

//    return view('client-name',[
//       'id'=>$id,
//       'name'=>$name
//    ]);


});

Route::get('if-for', function () {
    return view('if-for');
});*/


//Route::get('minharota',function(){
//    return 'Minha Rota';
//});
//
//Route::get('minharotafake.php',function(){
//    return 'Minha Rota Fake.php';
//});
//
//// {param}, {paramOpcional?}
//Route::get('client/{id}/{name?}', function($id,$name="Emílio"){
//    echo $id . ' = '. $name;
//});
//
//Route::get('formulario_csrf',function (){
//
//    $csrf_token = csrf_token();
//
//    $html = <<<HTML
//    <html>
//        <body>
//            <form action="/formulario_csrf" method="post">
//                <input type="hidden" name="_token" value="{$csrf_token}" />
//                <input type="text" name="value" />
//                <button type="submit">Enviar</button>
//            </form>
//        </body>
//    </html>
//HTML;
//
//   return $html;
//
//});
//
//Route::post('formulario_csrf',function(\Illuminate\Http\Request $request){
////    var_dump(json_encode($_POST));
//    $posted = json_encode($request->all());
//    return "Dados Postados pelo formulario_csrf: <hr/> {$posted}";
//})->name('client.store');
//
//// Rotas nomeadas
//Route::get('formulario2',function (){
//
//    $csrf_token = csrf_token();
//    $action = route('client.store');
//
//    $html = <<<HTML
//    <html>
//        <body>
//            <form action="{$action}" method="post">
//                <input type="hidden" name="_token" value="{$csrf_token}" />
//                <input type="text" name="value" />
//                <button type="submit">Enviar</button>
//            </form>
//        </body>
//    </html>
//HTML;
//
//    return $html;
//
//});
